import employees.Employee;
import employees.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EmplyeeServiceTest {

    @Test
    public void createAngajat () {
        String nume = "Ion";
        int varsta = 20;
        float salariu = 2000.0f;
        EmployeeService locatieTest = new EmployeeService("Test");
        Employee result = locatieTest.createAngajat(nume,varsta,salariu);

        Assertions.assertEquals(nume, result.getName());
        Assertions.assertEquals(varsta, result.getAge());
        Assertions.assertEquals(salariu, result.getSalary());

    }
}
