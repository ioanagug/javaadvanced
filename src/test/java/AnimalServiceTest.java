import animal.Animal;
import animal.AnimalService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class AnimalServiceTest {

    AnimalService service = new AnimalService("Test");

    @Test
    public void animalCreateTest () {
        //given
        String animalName = "Geroge";
        String animalSpecies = "Tigru";
        int animalAge = 3;

        //when
        Animal animal = service.createAnimal(animalName, animalSpecies, animalAge);
        //then
        Assertions.assertEquals(animalName, animal.getName());
        Assertions.assertEquals(animalSpecies, animal.getSpecies());
        Assertions.assertEquals(animalAge, animal.getAge());

        Assertions.assertEquals(1, AnimalService.contor);
        Assertions.assertEquals(animalName, AnimalService.animals[0].getName());
        Assertions.assertEquals(animalSpecies, AnimalService.animals[0].getSpecies());
        Assertions.assertEquals(animalAge, AnimalService.animals[0].getAge());


    }
}
