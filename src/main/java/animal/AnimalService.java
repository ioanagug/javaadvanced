package animal;

import java.util.Scanner;

public class AnimalService {

    public static int contor = 0;
    public static Animal[] animals = new Animal[100];
    private String location;
    private Animal[] localAnimals;
    private int localContor;

    public AnimalService(String locationParam) {
        this.location = locationParam;
        localAnimals = new Animal[100];
        localContor = 0;
    }

    public Animal getUserToCreateAnimal() {
        System.out.println("Please insert animal name:");
        Scanner keyboard = new Scanner(System.in);
        String animalName = keyboard.nextLine();
        String animalSpecies = keyboard.nextLine();
        int animalAge = keyboard.nextInt();

        Animal animal = createAnimal(animalName,animalSpecies,animalAge);
        return animal;
    }

    public Animal createAnimal (String nume, String specia, int varsta) {
        Animal animal = new Animal(nume, specia, varsta);
        animals[contor] = animal;
        contor++;
        System.out.println("Saved animal is: " + animal.toString());
        return animal;
    }

    public void displayAllAnimals() {
        for (int i = 0; i < contor; i++) {
            System.out.println(animals[i].toString());
        }
    }

    public void displayLocalAnimals() {
        for (int i = 0; i < localContor; i++) {
            System.out.println(animals[i].toString());
        }
    }

    public void editAnimal() {
        System.out.println("Dati numele animalului");
        Scanner keyboard = new Scanner(System.in);
        String numeAnimal = keyboard.nextLine();


        int pozizitieLocalAnimal = -1;

        // găsim animalul în localAnimals
        for (int i = 0; i < localContor; i++) {
            Animal localAnimal = localAnimals[i];
            if (localAnimal.getName().equals(numeAnimal)) {
                pozizitieLocalAnimal = i;
            }
        }

        if (pozizitieLocalAnimal == -1) {
            System.out.println("Nu există niciun animal pe nume " + numeAnimal + " la " + location);
            return;
        }

        Animal animalCuDateVechi = localAnimals[pozizitieLocalAnimal];

        int pozitieStaticAnimals = -1;
        // gasim animalul in animals (cele static)
        // presupunem ca
        for (int i = 0; i < contor; i++) {
            Animal currentAnimal = animals[i];
            if (currentAnimal.getName().equals(animalCuDateVechi.getName()) &&
                    currentAnimal.getSpecies().equals(animalCuDateVechi.getSpecies()) &&
                    currentAnimal.getAge() == animalCuDateVechi.getAge()) {
                pozitieStaticAnimals = i;
            }
        }

        if (pozitieStaticAnimals == -1) {
            System.out.println("Nju există niciun animal pe numele " + numeAnimal + " global");
            return;
        }

        System.out.println("Scrieți noile date: nume, specie și vârstă pentru " + animalCuDateVechi.toString());
        String noulNume = keyboard.nextLine();
        String nouaSpecie = keyboard.nextLine();
        int nouaVârstă = keyboard.nextInt();


        localAnimals[pozizitieLocalAnimal].setName(noulNume);
        localAnimals[pozizitieLocalAnimal].setSpecies(nouaSpecie);
        localAnimals[pozizitieLocalAnimal].setAge(nouaVârstă);


        animals[pozitieStaticAnimals].setName(noulNume);
        animals[pozitieStaticAnimals].setSpecies(nouaSpecie);
        animals[pozitieStaticAnimals].setAge(nouaVârstă);
    }


}

