package animal;

public class Animal {

    private String name;
    private String species;
    private int age;
    public static boolean visitable;

    public Animal(String name, String species, int age) {
        this.name = name;
        this.species = species;
        this.age = age;
        this.visitable = visitable;
    }

    public String getName () {
        return name;
    }

    public String getSpecies() {
        return species;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animalul salvat este " + name +
                ", specia " + species +
                ", varsta " + age +
                ", vizitabil " + visitable;
    }
}
