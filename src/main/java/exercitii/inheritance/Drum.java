package exercitii.inheritance;

public class Drum extends Constructie{
    private int lungime;
    private int nrBenzi;

    public Drum(int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int lungime, int nrBenzi) {
        super(costProiectare, costConstructie, material, tipConstructie);
        this.lungime = lungime;
        this.nrBenzi = nrBenzi;
    }

    public int getLungime() {
        return lungime;
    }

    public int getNrBenzi() {
        return nrBenzi;
    }
}
