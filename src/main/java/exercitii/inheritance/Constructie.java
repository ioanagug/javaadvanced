package exercitii.inheritance;

public class Constructie {
    private int costProiectare;
    private int costConstructie;
    private Material material;
    private TipConstructie tipConstructie;


    public Constructie(int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie) {
        this.costProiectare = costProiectare;
        this.costConstructie = costConstructie;
        this.material=material;
        this.tipConstructie=tipConstructie;
    }

    public int getCostProiectare() {
        return costProiectare;
    }

    public int getCostConstructie() {
        return costConstructie;
    }

    public Material getMaterial() {
        return material;
    }

    public TipConstructie getTipConstructie() {
        return tipConstructie;
    }

}

