package exercitii.inheritance;

public class Casa extends Cladire {
    private int nrCamere;
    //simpla, duplex, triplex, casa inlantuita

    public Casa(int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int nrEtaje, float suprafata, String adresa, int nrCamere) {
        super(costProiectare, costConstructie, material, tipConstructie, nrEtaje, suprafata, adresa);
        this.nrCamere = nrCamere;
    }

    public int getNrCamere() {
        return nrCamere;
    }
}
