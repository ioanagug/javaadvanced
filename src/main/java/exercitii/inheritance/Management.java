package exercitii.inheritance;

public class Management {
    public static void main(String[] args) {
        Casa duplex = new Casa(25000, 3000,Material.MATERIALE_ECO, TipConstructie.PRIVAT, 2, 160, "Florilor", 5);
        Bloc bloc = new Bloc(50000, 60000, Material.CARAMIDA, TipConstructie.PRIVAT, 6, 600, "Primaverii", 28);
        Hala hala = new Hala(5000, 70000, Material.STRUCTURA_METALICA, TipConstructie.PRIVAT, 1,600, "Clujului", "depozit");
        Pod pod = new Pod(90000, 85000, Material.BETON, TipConstructie.PUBLIC, 7, 100, TipPod.PE_PILONI);
        Drum drum = new Drum(150000, 600000, Material.BETON, TipConstructie.PUBLIC, 1000, 4);

        Constructie[] sirConstructii = new Constructie[100];
        sirConstructii[0] = duplex;
        sirConstructii[1] = bloc;
        sirConstructii[2] = hala;
        sirConstructii[3] = pod;
        sirConstructii[4] = drum;

        int sumCons = 0;
        int sumProiect = 0;


        for (int i = 0; i < sirConstructii.length; i++) {
            if (sirConstructii[i]!=null) {
                sumCons = sumCons + sirConstructii[i].getCostConstructie();
                sumProiect = sumProiect + sirConstructii[i].getCostProiectare();
            }
        }
        System.out.println("Suma costurilor de constructie este " + sumCons);
        System.out.println("Suma costurilor de proiectare este " + sumProiect);

        int maxLocal = sirConstructii[0].getCostConstructie();
        for (int i= 0; i<=4; i++) {
            if (sirConstructii[i].getCostConstructie()>maxLocal) {
                maxLocal=sirConstructii[i].getCostConstructie();
            }
        }
        System.out.println("Costul maxim de constructie este " + maxLocal);
        //daca costProiect>50%costConstr => cladirea ilegala
        // cate cladiri ilegale => afis
        int nrCladiriIlegale = 0;
        for (int i=0; i<=4; i++) {
            if (sirConstructii[i].getCostProiectare()>0.5*sirConstructii[i].getCostConstructie()) {
                System.out.println(sirConstructii[i].getClass());
                nrCladiriIlegale++;
            }
        }
        System.out.println("Nr de cladiri ilegale este "+ nrCladiriIlegale);

    }
}
