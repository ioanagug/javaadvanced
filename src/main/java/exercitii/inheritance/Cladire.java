package exercitii.inheritance;

public class Cladire extends Constructie {
    private int nrEtaje;
    private float suprafata;
    private String adresa;

    public Cladire(int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int nrEtaje, float suprafata, String adresa) {
        super(costProiectare, costConstructie, material, tipConstructie);
        this.nrEtaje = nrEtaje;
        this.suprafata = suprafata;
        this.adresa = adresa;
    }

    public int getNrEtaje() {
        return nrEtaje;
    }

    public float getSuprafata() {
        return suprafata;
    }

    public String getAdresa() {
        return adresa;
    }
}
