package exercitii.inheritance;

public class Bloc extends Cladire {
    private int nrApt;

    public Bloc (int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int nrEtaje, float suprafata, String adresa, int nrApt) {
        super(costProiectare, costConstructie, material, tipConstructie, nrEtaje, suprafata, adresa);
        this.nrApt=nrApt;
    }

    public int getNrApt() {
        return nrApt;
    }
}
