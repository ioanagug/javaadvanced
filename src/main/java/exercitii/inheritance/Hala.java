package exercitii.inheritance;

public class Hala extends Cladire {
    private String utilizare;

    public Hala (int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int nrEtaje, float suprafata, String adresa, String utilizare) {
        super(costProiectare, costConstructie, material, tipConstructie, nrEtaje, suprafata, adresa);
        this.utilizare=utilizare;
    }

    public String getUtilizare() {
        return utilizare;
    }
}
