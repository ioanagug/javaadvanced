package exercitii.inheritance;

public class Pod extends Constructie {
    private int inaltime;
    private int lungime;
    private TipPod tipPod;

    public Pod(int costProiectare, int costConstructie, Material material, TipConstructie tipConstructie, int inaltime, int lungime, TipPod tipPod) {
        super(costProiectare, costConstructie, material, tipConstructie);
        this.inaltime = inaltime;
        this.lungime = lungime;
        this.tipPod= tipPod;
    }

    public int getInaltime() {
        return inaltime;
    }

    public int getLungime() {
        return lungime;
    }
}
