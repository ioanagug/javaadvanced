import employees.EmployeeService;

import java.util.Scanner;

public class Management {
    public static void main(String[] args) {

        EmployeeService locatieOradea = new EmployeeService("Oradea");
        EmployeeService locatieTgMures = new EmployeeService("TgMures");
        EmployeeService locatieBucuresti = new EmployeeService("Bucuresti");
        System.out.println("Introduceti angajati pt Oradea: ");
        locatieOradea.getAngajatFromUSer();
        locatieOradea.getAngajatFromUSer();
        locatieOradea.arataAngajatiLocal();
        System.out.println("Introduceti angajati pt Tg. Mures: ");
        locatieTgMures.getAngajatFromUSer();
        locatieTgMures.arataAngajatiLocal();
        System.out.println("Introduceti angajati pt Bucuresti: ");
        locatieBucuresti.getAngajatFromUSer();
        locatieBucuresti.arataAngajatiLocal();
        locatieTgMures.arataTotiAngajatii();

        Scanner keyboard = new Scanner(System.in);
        System.out.println("Alegeti locatia noului angajat: ");
        System.out.println("1. Oradea");
        System.out.println("2. Tg. Mures");
        System.out.println("3. Bucuresti");
        int alegere = keyboard.nextInt();
        switch (alegere) {
            case 1:
                locatieOradea.editareAngajat();
                break;
            case 2:
                locatieTgMures.editareAngajat();
                break;
            case 3:
                locatieBucuresti.editareAngajat();
                break;
        }

        locatieBucuresti.arataTotiAngajatii();



    }
}
