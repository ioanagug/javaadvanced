import animal.AnimalService;

import java.util.Scanner;

public class ZooController {

    public static void main(String[] args) {
        AnimalService animalServiceTgMures = new AnimalService("Tg Mures");
        AnimalService animalServiceOradea = new AnimalService("Oradea");
        AnimalService animalServiceBucuresti = new AnimalService("Bucuresti");

        //tipul_obiectului nume_obiect = new constructor

        System.out.println("Animale din bucuresti: ");
        animalServiceBucuresti.getUserToCreateAnimal();
        animalServiceBucuresti.getUserToCreateAnimal();
        System.out.println("Animale din Oradea: ");
        animalServiceOradea.getUserToCreateAnimal();
        animalServiceOradea.getUserToCreateAnimal();
        animalServiceOradea.getUserToCreateAnimal();
        System.out.println("Animale din Tg Mures");
        animalServiceTgMures.getUserToCreateAnimal();

        animalServiceBucuresti.displayLocalAnimals();
        animalServiceBucuresti.displayAllAnimals();

        animalServiceOradea.displayLocalAnimals();
        animalServiceOradea.displayAllAnimals();

        System.out.println("----------------------------------------");
        System.out.println("Selectați locația: ");
        System.out.println("1. București");
        System.out.println("2. Oradea");
        System.out.println("3. Tg Mureș");
        Scanner keyboard = new Scanner(System.in);
        int optiuneOras = keyboard.nextInt();

        switch (optiuneOras) {
            case 1:
                animalServiceBucuresti.editAnimal();
                break;
            case 2:
                animalServiceOradea.editAnimal();
                break;
            case 3:
                animalServiceTgMures.editAnimal();
                break;
        }
        animalServiceOradea.displayAllAnimals();
    }
}
