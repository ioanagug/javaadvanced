package employees;

import java.util.Scanner;

public class EmployeeService {

    public static Employee[] sirAngajati = new Employee[100];
    public static int contor=0;
    private Employee[] sirLocalAngajati;
    private int contorLocal;
    private String locatie;

    public EmployeeService(String locatie) {
        this.locatie = locatie;
        contorLocal=0;
        sirLocalAngajati=new Employee[100];
    }

    public Employee createAngajat (String nume, int varsta, float salariu) {
        Employee angajatNou = new Employee(nume, varsta, salariu);
        sirAngajati[contor] = angajatNou;
        contor++;
        sirLocalAngajati[contorLocal]=angajatNou;
        contorLocal++;
        return angajatNou;
    }

    public Employee getAngajatFromUSer () {
        System.out.println("Introduceti datele angajatului: nume, varsta, salariul ");
        Scanner keyboard = new Scanner(System.in);
        String nume = keyboard.nextLine();
        int varsta = keyboard.nextInt();
        float salariu = keyboard.nextFloat();
        Employee angajatNou = new Employee(nume, varsta, salariu);
        createAngajat(nume, varsta, salariu);
        return angajatNou;
    }

    public void arataTotiAngajatii () {
        System.out.println("Lista angajatilor de la ZooRO: ");
        for (int i=0; i<contor; i++) {
            System.out.println(sirAngajati[i].toString());
        }
    }
    public void arataAngajatiLocal () {
        System.out.println("Lista locala a angajatilor Zoo " + locatie);
        for (int i=0; i<contorLocal; i++) {
            System.out.println(sirLocalAngajati[i].toString());
        }
    }

    public void editareAngajat () {
        System.out.println("Introduceti angajatul vechi cu: nume, varsta, salariu");
        Scanner keyboard = new Scanner(System.in);
        String vechiulNume=keyboard.nextLine();
        int vecheaVarsta=keyboard.nextInt();
        float vechiulSalariu = keyboard.nextFloat();
        System.out.println("Introduceti angajatul nou cu: nume, varsta, salariu");
        String noulNume=keyboard.next();
        int nouaVarsta=keyboard.nextInt();
        float noulSalariu = keyboard.nextFloat();

        for (int i=0; i<contorLocal; i++) {
            if ((sirLocalAngajati[i].getName().equals(vechiulNume) &&
                    sirLocalAngajati[i].getAge() == vecheaVarsta &&
                    sirLocalAngajati[i].getSalary() == vechiulSalariu)) {
                sirLocalAngajati[i].setName(noulNume);
                sirLocalAngajati[i].setAge(nouaVarsta);
                sirLocalAngajati[i].setSalary(noulSalariu);
            }
        }
        for (int i=0; i<contor; i++) {
            if (sirAngajati[i].getName().equals(vechiulNume) &&
            sirAngajati[i].getAge() == vecheaVarsta &&
            sirAngajati[i].getSalary() == vechiulSalariu) {
                sirAngajati[i].setName(noulNume);
                sirAngajati[i].setAge(nouaVarsta);
                sirAngajati[i].setSalary(noulSalariu);
            }
        }

    }
}
